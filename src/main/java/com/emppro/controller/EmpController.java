package com.emppro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.emppro.dto.EmpRequestDto;
import com.emppro.dto.EmpResponseDto;
import com.emppro.service.EmpService;

@RestController
public class EmpController {

	@Autowired
	EmpService empservice;
	
	@PostMapping
	public String addEmp(EmpRequestDto employeeDetailsRequest) {
		
		empservice.addingnewEmployee(employeeDetailsRequest);
		return "Record Added To Database Successfully";

	}
	
	@GetMapping
	public EmpResponseDto getEmpDetails(@RequestParam int empId) {
		return empservice.getEmployees(empId);
	}
}
