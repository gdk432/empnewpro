package com.emppro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpproApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpproApplication.class, args);
	}

}
