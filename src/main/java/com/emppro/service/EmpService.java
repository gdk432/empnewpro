package com.emppro.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.emppro.dto.EmpRequestDto;
import com.emppro.dto.EmpResponseDto;
import com.emppro.entity.Employee;
import com.emppro.repository.EmpRepository;

@Service
public class EmpService {

	@Autowired
	EmpRepository empRepo;
	
	public void addingnewEmployee(EmpRequestDto employeeDetailsRequest ) {
		Employee emp = new Employee();
		emp.setEmpName(employeeDetailsRequest.getEmpName());
		emp.setEmpPhone(employeeDetailsRequest.getEmpPhone());
		emp.setEmpSal(employeeDetailsRequest.getEmpSal());
		emp.setEmpCity(employeeDetailsRequest.getEmpCity());
		emp.setJoiningDate("06-16-2021");
		empRepo.save(emp);
		
	}

	public EmpResponseDto getEmployees(int empId) {
		Employee empresponse = new Employee();
		EmpResponseDto resposneDto = new EmpResponseDto();
		empresponse=empRepo.getById(empId);
		
		resposneDto.setEmpId(empresponse.getEmpId());
		resposneDto.setEmpCity(empresponse.getEmpCity());
		resposneDto.setEmpName(empresponse.getEmpName());
		resposneDto.setEmpPhone(empresponse.getEmpPhone());
		resposneDto.setEmpSal(empresponse.getEmpSal());
		resposneDto.setJoiningDate(empresponse.getJoiningDate());
		return resposneDto;
	}


}
